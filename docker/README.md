## Installation

Clone repo to `/var/cache/mesa_ci_results`

Create a symlink under /etc/docker-compose:
    ln -sf /var/cache/mesa_ci_results/docker /etc/docker-compose

Enable the systemd service:
    sudo systemctl enable /etc/docker-compose/docker-compose.service

Create a password file for sql (e.g. using pwgen) and place it in a new
file: `/etc/docker-compose/secrets/mysql-pw-jenkins`

### Proxy/DNS config

`/etc/systemd/system/docker.service.d/proxy.conf`:
```
[Service]
Environment="HTTP_PROXY=http://proxy.jf.intel.com:911"
Environment="HTTPS_PROXY=http://proxy.jf.intel.com:911"
```
`/etc/docker/daemon.json`:

```
{
        "storage-driver": "overlay2",
        "dns": ["10.248.2.1", "10.22.224.204"]
}
```

Note: overlay2 is supposedly a more performance storage driver.
